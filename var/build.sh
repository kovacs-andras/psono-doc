#!/usr/bin/env bash
apt-get update && \
node --version && \
npm --version && \
yarn --version && \
yarn install && \
yarn docs:build && \
./node_modules/raml2html/bin/raml2html api/api.raml > build/api.html && \
./node_modules/raml2html/bin/raml2html --theme 'raml2html-slate-theme' -i 'api/api.raml' -o 'build/api2.html'