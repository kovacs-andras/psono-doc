---
title: Usage Psonoci
metaTitle: Usage Psonoci | Psono Documentation
meta:
  - name: description
    content: How to use Psonoci in your build pipeline
---

# Usage Psonoci (Beta)

The basic command looks like this:

``` bash
psonoci [OPTIONS] <SUBCOMMAND> [FLAGS]
```

## General `OPTIONS`

`OPTIONS` can either be specified in the command or with environment variables and need to be set in all subcommands
(with the exception of `help` of course). All commands accept the following `OPTIONS`.

| Option | Description | Environment Variable | Required |
|--------|-------------|----------------------|----------|
| --api-key-id | api key as uuid | PSONO_CI_API_KEY_ID | yes |
| --api-secret-key-hex | api secret key as 64 byte hex string | PSONO_CI_API_SECRET_KEY_HEX | yes |
| --server-url | Url of the psono backend server | PSONO_CI_SERVER_URL | yes |
| --timeout | Connection timeout in seconds  [default: 60] | PSONO_CI_TIMEOUT | no |


## General `FLAGS`

All commands accept the following `FLAGS`

| Option | Description |
|--------|-------------|
| -h, --help | Prints help information |
| -V, --version | Prints version information |


## Overview Subcommands

Psonoci accepts currently the following subcommands.

| Option | Description |
|--------|-------------|
| help | Prints the help for a specific command |
| secret get | Used to retrieve a specific secret, identified by its ID. |

### Example

``` bash
psonoci help
psonoci secret get --help
```




## Subcommand `help`
Used to print the help for a specific command. It can also be passed in as a `FLAG` with `--help`

### Example

``` bash
psonoci help
```



## Subcommand `secret get`

Used to retrieve a specific secret, identified by its ID.

### Arguments:

| Option | Description |
|--------|-------------|
| secret-id | The secret's uuid, e.g. `25070c66-8950-4264-9b39-11e6d83312e3` |
| secret-value | Which secret value to return (`json` returns all values in a json object). Possible values: `json`, `notes`, `password`, `title`, `url`, `url_filter`, `username`, `gpg_key_email`, `gpg_key_name`, `gpg_key_private`, `gpg_key_public` |

### Example with environment variables

``` bash
export PSONO_CI_API_KEY_ID=eaee77c6-169f-4873-9be3-f2613149baa9
export PSONO_CI_API_SECRET_KEY_HEX=c8321d7a8e5b1f5ec3d969ecb5054c7548a1c709ddab2edae2ff7ff028538917
export PSONO_CI_SERVER_URL=https://example.com/server

psonoci secret get 25070c66-8950-4264-9b39-11e6d83312e3 json
```

### Example without environment variables

``` bash
psonoci \
    --api-key-id eaee77c6-169f-4873-9be3-f2613149baa9 \
    --api-secret-key-hex c8321d7a8e5b1f5ec3d969ecb5054c7548a1c709ddab2edae2ff7ff028538917 \
    --server-url https://example.com/server \
    secret get 25070c66-8950-4264-9b39-11e6d83312e3 json
```




## Subcommand `secret set`

Used to update a specific secret, identified by its ID.

### Arguments:

| Option | Description |
|--------|-------------|
| secret-id | The secret's uuid, e.g. `25070c66-8950-4264-9b39-11e6d83312e3` |
| secret-value | Which secret value-type to set ('json' not yet supported). Possible values: `json`, `notes`, `password`, `title`, `url`, `url_filter`, `username`, `gpg_key_email`, `gpg_key_name`, `gpg_key_private`, `gpg_key_public`, `secret_type`, `env_vars` |
| secret-new-value | The new value to set for type |

### Example with environment variables

``` bash
export PSONO_CI_API_KEY_ID=eaee77c6-169f-4873-9be3-f2613149baa9
export PSONO_CI_API_SECRET_KEY_HEX=c8321d7a8e5b1f5ec3d969ecb5054c7548a1c709ddab2edae2ff7ff028538917
export PSONO_CI_SERVER_URL=https://example.com/server

psonoci secret set 25070c66-8950-4264-9b39-11e6d83312e3 password newPassword
```

### Example without environment variables

``` bash
psonoci \
    --api-key-id eaee77c6-169f-4873-9be3-f2613149baa9 \
    --api-secret-key-hex c8321d7a8e5b1f5ec3d969ecb5054c7548a1c709ddab2edae2ff7ff028538917 \
    --server-url https://example.com/server \
    secret set 25070c66-8950-4264-9b39-11e6d83312e3 password newPassword
```





## Subcommand `run`

Spawns processes with environment variables from the api-keys secrets. For this to work you need create an environment variables secret

![Create environment variables secret](/images/user/psonoci/env_vars_create.png)

and then grant access to the api key for the environment variable secret.

![Grant access to the environment variables secret](/images/user/psonoci/env_vars_api_key.png)

### Arguments:

| Option | Description |
|--------|-------------|
| command-values | The actual to run e.g. `./some_command --timeout=10` |

### Example with environment variables

``` bash
export PSONO_CI_API_KEY_ID=eaee77c6-169f-4873-9be3-f2613149baa9
export PSONO_CI_API_SECRET_KEY_HEX=c8321d7a8e5b1f5ec3d969ecb5054c7548a1c709ddab2edae2ff7ff028538917
export PSONO_CI_SERVER_URL=https://example.com/server

psonoci run -- ./some_command
```

### Example without environment variables

``` bash
psonoci \
    --api-key-id eaee77c6-169f-4873-9be3-f2613149baa9 \
    --api-secret-key-hex c8321d7a8e5b1f5ec3d969ecb5054c7548a1c709ddab2edae2ff7ff028538917 \
    --server-url https://example.com/server \
    run -- ./some_command
```


## Subcommand `config save`

psonoci can generate a single config file, containing the api key id, secret and server url and later be used to simplify commands.

### Arguments:

| Option | Description |
|--------|-------------|
| path | Output path where to store the config e.g. `/path/to/config.toml` |

### Example without environment variables

``` bash
psonoci \
    --api-key-id eaee77c6-169f-4873-9be3-f2613149baa9 \
    --api-secret-key-hex c8321d7a8e5b1f5ec3d969ecb5054c7548a1c709ddab2edae2ff7ff028538917 \
    --server-url https://example.com/server \
    config save /path/to/config.toml
```

This creates `/path/to/config.toml` and then can later be passed in as s single parameter

``` bash
psonoci -c /path/to/config.toml secret get 25070c66-8950-4264-9b39-11e6d83312e3 json
```

 or as an environment variable

``` bash
export PSONO_CI_CONFIG_PATH="/path/to/config.toml"
psonoci secret get 25070c66-8950-4264-9b39-11e6d83312e3 json
```
