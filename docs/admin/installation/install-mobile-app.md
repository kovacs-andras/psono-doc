---
title: 7. Install Mobile App (optional)
metaTitle: Install Psono Mobile App | Psono Documentation
meta:
  - name: description
    content: Install instruction for the Psono mobile app for Android
---

# Install Mobile App

Install instruction for the Psono mobile app for Android.

## Preamble

At this point we assume that you already have a psono server running, ready to accept connections. Mobile apps are a
more convenient way to access secrets, yet offer only a limited number of functionality compared to the web client.
In addition please be aware that the app won't work with custom CAs, so you need a "global trusted" certificate like e.g. letsencrypt.

## Installation for Android

You can find the Psono Android App in the Google Play store here:

[Psono Android App](https://play.google.com/store/apps/details?id=com.psono.psono)


## Installation for iOS

You can find the Psono iOS App in the Apple's App Store here:

[Psono iOS App](https://apps.apple.com/us/app/psono-password-manager/id1545581224)


## Configuration with QR code

The mobile app allows to be configured via QR config.

1)  Create QR Config

    Use any client (a normal web client or a browser extension) to login, and afterwards go to `Account -> Overview` and click
    the `Show` button

    ![Create the QR Config](/images/admin/install_mobile_app/create_qr_config.jpg)

    This QR config contains the `config.json` that the current client is using.

2)  Scan QR Config

    Open the app and click the `Scan Config?` button at the bottom.

    ![Scan Config](/images/admin/install_mobile_app/scan_config.jpg)

    ::: tip
    If you want to remove the configuration, you can delete and reinstall the app or scan another valid QR config.
    :::
