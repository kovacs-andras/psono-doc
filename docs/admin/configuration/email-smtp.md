---
title: Email - SMTP
metaTitle: Email configuration with SMTP | Psono Documentation
meta:
  - name: description
    content: Configuration of email delivery with SMTP
---

# Email configuration with SMTP

## Preamble

The server supports multiple email providers. This guide will explain how to configure the Psono server to use SMTP for
email delivery.

## Configuration

During the installation of the server you have created a settings.yaml that needs to be adjusted now.

1.  Configure email address

	```
	EMAIL_FROM: 'something@example.com'
	```

	All emails that are sent by the server will come from this email address.

    Restart the server afterwards

2.  Adjust the following SMTP credentials in your setting.yml

	```
	EMAIL_BACKEND: 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_HOST: 'YOUR_EMAIL_HOST'
    EMAIL_HOST_USER: 'YOUR_EMAIL_HOST_USER'
    EMAIL_HOST_PASSWORD: 'YOUR_EMAIL_HOST_PASSWORD'
    EMAIL_PORT: 'YOUR_EMAIL_PORT'
    EMAIL_USE_TLS: 'YOUR_EMAIL_USE_TLS'
    EMAIL_USE_SSL: 'YOUR_EMAIL_USE_SSL'
    EMAIL_SSL_CERTFILE: 'YOUR_EMAIL_SSL_CERTFILE'
    EMAIL_SSL_KEYFILE: 'YOUR_EMAIL_SSL_KEYFILE'
    EMAIL_TIMEOUT: 'YOUR_EMAIL_TIMEOUT'
	```

	- Replace `YOUR_EMAIL_HOST` with your SMTP's host. e.g. localhost or 192.168.0.5
	- Replace `YOUR_EMAIL_HOST_USER` with your SMTP's user.
	- Replace `YOUR_EMAIL_HOST_PASSWORD` with your SMTP's password.
	- Replace `YOUR_EMAIL_PORT` with your SMTP's port, e.g. 25
	- Replace `YOUR_EMAIL_USE_TLS` with true or false. Whether to use a TLS (secure) connection when talking to the SMTP server. This is used for explicit TLS connections, generally on port 587. If you are experiencing hanging connections, see the implicit TLS setting EMAIL_USE_SSL.
        ::: tip
        EMAIL_USE_TLS/EMAIL_USE_SSL are mutually exclusive, so only set one of those settings to True.
        :::
	- Replace EMAIL_USE_SSL with true or false. Whether to use an implicit TLS (secure) connection when talking to the SMTP server. In most email documentation this type of TLS connection is referred to as SSL. It is generally used on port 465. If you are experiencing problems, see the explicit TLS setting EMAIL_USE_TLS.
        ::: tip
        EMAIL_USE_TLS/EMAIL_USE_SSL are mutually exclusive, so only set one of those settings to True.
        :::
	- (optional) Replace YOUR_EMAIL_SSL_CERTFILE (If EMAIL_USE_SSL or EMAIL_USE_TLS is True) with the path to a PEM formatted certificate chain file to use for the SSL connection
	- (optional) Replace YOUR_EMAIL_SSL_KEYFILE (If EMAIL_USE_SSL or EMAIL_USE_TLS is True) with the path to a PEM formatted key file to use for the SSL connection.
        ::: tip
        Setting EMAIL_SSL_CERTFILE and EMAIL_SSL_KEYFILE doesn’t result in any certificate checking. They’re passed to the underlying SSL connection. Please refer to the documentation of Python’s ssl.wrap_socket() function for details on how the certificate chain file and private key file are handled.
        :::
	- (optional) Replace EMAIL_TIMEOUT to specify a timeout in seconds for blocking operations like the connection attempt.

    Restart the server afterwards


## Testing

To send a test email to `something@something.com` execute:

    python3 ./psono/manage.py sendtestmail something@something.com

or with docker:

    docker run --rm \
      -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml \
      -ti psono/psono-server:latest python3 ./psono/manage.py sendtestmail something@something.com

If you receive this test email, then email should be configured proper.


## More Information

Psono is using standard django under the hood for SMTP. You can check out the official documentation here:

[docs.djangoproject.com/en/2.2/ref/settings/](https://docs.djangoproject.com/en/2.2/ref/settings/)

## Example Office365

For Office365 you can use the following settings:

    EMAIL_FROM: "something@example.com"
    EMAIL_BACKEND: 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_HOST: "smtp.outlook.office365.com"
    EMAIL_HOST_USER: "something@example.com"
    EMAIL_HOST_PASSWORD: "Your password"
    EMAIL_PORT: 587
    EMAIL_USE_TLS: True
