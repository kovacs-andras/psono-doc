---
title: Custom Branding
metaTitle: Custom Branding | Psono Documentation
meta:
  - name: description
    content: This guide will explain how to use custom branding.
---

# Custom Branding

## Preamble

This guide explains how to customize the look and feel of Psono. You can change logos, links and styles.


## Client

### Top Logo

1.  Upload the logo

First upload the new logo to e.g. /opt/docker/psono-client/top_logo.png. The original logo has a resolution of 172x35 px.

2.  Change start command

	```bash
    docker run --name psono-client \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/config.json \
        -d --restart=unless-stopped -p 10101:80 psono/psono-client:latest
	```

	Adjust this to include:

	```bash
    ...
        -v /opt/docker/psono-client/top_logo.png:/usr/share/nginx/html/img/logo-inverse.png \
    ...
	```
    Restart the client afterwards

### Sign in Logo

1.  Upload the logo

    First upload the new logo to e.g. /opt/docker/psono-client/sign_in_logo.png. The original logo has a resolution of 172x35 px.

2.  Change start command

	```bash
    docker run --name psono-client \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/config.json \
        -d --restart=unless-stopped -p 10101:80 psono/psono-client:latest
	```

	Adjust this to include:

	```bash
    ...
        -v /opt/docker/psono-client/sign_in_logo.png:/usr/share/nginx/html/img/logo.png \
    ...
	```
    Restart the client afterwards

### Favicons

1.  Upload the favicons

    Psono is using favicons in multiple resolutions. Upload them to e.g. /opt/docker/psono-client. A list of the available
    files:
    
    - android-chrome-192x192.png
    - android-chrome-512x512.png
    - apple-touch-icon.png
    - appstore_apple.png
    - appstore_google.png
    - favicon-16x16.png
    - favicon-32x32.png
    - favicon.ico
    - icon-128.png
    - icon-16.png
    - icon-32.png
    - icon-48.png
    - icon-64.png
    - mstile-150x150.png

2.  Change start command

	```bash
    docker run --name psono-client \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/config.json \
        -d --restart=unless-stopped -p 10101:80 psono/psono-client:latest
	```

	Adjust this to include your files e.g.:

	```bash
    ...
        -v /opt/docker/psono-client/android-chrome-192x192.png:/usr/share/nginx/html/img/android-chrome-192x192.png \
        -v /opt/docker/psono-client/android-chrome-512x512.png:/usr/share/nginx/html/img/android-chrome-512x512.png \
        -v /opt/docker/psono-client/apple-touch-icon.png:/usr/share/nginx/html/img/apple-touch-icon.png \
        -v /opt/docker/psono-client/appstore_apple.png:/usr/share/nginx/html/img/appstore_apple.png \
        -v /opt/docker/psono-client/appstore_google.png:/usr/share/nginx/html/img/appstore_google.png \
        -v /opt/docker/psono-client/favicon-16x16.png:/usr/share/nginx/html/img/favicon-16x16.png \
        -v /opt/docker/psono-client/favicon-32x32.png:/usr/share/nginx/html/img/favicon-32x32.png \
        -v /opt/docker/psono-client/favicon.ico:/usr/share/nginx/html/img/favicon.ico \
        -v /opt/docker/psono-client/icon-128.png:/usr/share/nginx/html/img/icon-128.png \
        -v /opt/docker/psono-client/icon-16.png:/usr/share/nginx/html/img/icon-16.png \
        -v /opt/docker/psono-client/icon-32.png:/usr/share/nginx/html/img/icon-32.png \
        -v /opt/docker/psono-client/icon-48.png:/usr/share/nginx/html/img/icon-48.png \
        -v /opt/docker/psono-client/icon-64.png:/usr/share/nginx/html/img/icon-64.png \
        -v /opt/docker/psono-client/mstile-150x150.png:/usr/share/nginx/html/img/mstile-150x150.png \
    ...
	```
    Restart the client afterwards

### Custom CSS

1.  Create custom CSS file

    You should start off with one of our existing CSS files and modify it e.g. datastore.css A list of all CSS files can be found here:
    https://gitlab.com/psono/psono-client/-/tree/master/src/common/data/css
    
    We assume that you have your modified copy present in e.g. /opt/docker/psono-client/datastore.css

2.  Change start command

	```bash
    docker run --name psono-client \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/config.json \
        -d --restart=unless-stopped -p 10101:80 psono/psono-client:latest
	```

	Adjust this to include:

	```bash
    ...
        -v /opt/docker/psono-client/datastore.css:/usr/share/nginx/html/css/datastore.css \
    ...
	```
    Restart the client afterwards
    
### Custom "More" links

There are a couple of links shown in the left sidebar in the "more" section. You can customize them.

1.  Modify config.json

    Usually you start off with a config.json like this:
    
    ```json
    {
      "backend_servers": [...],
      "base_url": "https://www.psono.pw/",
      ...
    }
    ```
        
    You can now modify it to show other more links like shown below:
    
	```json
	{
	  "backend_servers": [...],
	  "base_url": "https://www.psono.pw/",
	  ...
	  "more_links": [{
          "href": "https://support.example.com/",
          "title": "Company Support"
      }]
	}
	```
        
    If you want to add the original links to then this is how it would looks like:
	```json
	{
	  "backend_servers": [...],
	  "base_url": "https://www.psono.pw/",
	  ...
	  "more_links": [{
          "href": "https://doc.psono.com/",
          "title": "DOCUMENTATION",
          "class": "fa-book"
      },{
          "href": "privacy-policy.html",
          "title": "PRIVACY_POLICY",
          "class": "fa-user-secret"
      },{
          "href": "https://www.psono.com",
          "title": "ABOUT_US",
          "class": "fa-info-circle"
      }]
	}
	```
	
	Here you also see the usage of custom CSS classes to show custom icons. Psono currently supports Fontawesome 4.0 icons.
	
    Restart the client afterwards

## Server

### Logo in E-Mails

1.  Upload the logo

    First upload the new logo to e.g. /opt/docker/psono-server/email_logo.png

2.  Change start command 

	```bash
    docker run --name psono-server \
        --sysctl net.core.somaxconn=65535 \
        -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml \
        -v /path/to/log/folder:/var/log/psono \
        -d --restart=unless-stopped -p 10100:80 psono/psono-server-enterprise:latest
	```

	Adjust this to include:

	```bash
    ...
        -v /opt/docker/psono-server/email_logo.png:/root/psono/static/email/logo.png \
    ...
	```
    Restart the server afterwards
