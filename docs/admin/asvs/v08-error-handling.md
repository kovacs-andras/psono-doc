---
title: V8 Error Handling
metaTitle: V8 Error Handling | Psono Documentation
meta:
  - name: description
    content: Error handling and logging verification requirements
---

# V8 Error Handling

Error handling and logging verification requirements

## ASVS Verification Requirement

| ID                          | Detailed Verification Requirement                                                                                                                                                                                                                                                                                                                              | Level 1                 | Level 2    | Level 3     | Since     |
|-----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|------------|-------------|-----------|
| 8.1                         | Verify that the application does not output error messages or stack traces containing sensitive data that could assist an attacker, including session id, software/framework versions and personal information                                                                                                                                                 | x                       | x          | x           | 1.0       |
| 8.2                         | Verify that error handling logic in security controls denies access by default.                                                                                                                                                                                                                                                                                |                         | x          | x           | 1.0       |
| 8.3                         | Verify security logging controls provide the ability to log success and particularly failure events that are identified as security-relevant.                                                                                                                                                                                                                  |                         | x          | x           | 1.0       |
| 8.4                         | Verify that each log event includes necessary information that would allow for a detailed investigation of the timeline when an event happens.                                                                                                                                                                                                                 |                         | x          | x           | 1.0       |
| 8.5                         | Verify that all events that include untrusted data will not execute as code in the intended log viewing software.                                                                                                                                                                                                                                              |                         | x          | x           | 1.0       |
| 8.6                         | Verify that security logs are protected from unauthorized access and modification.                                                                                                                                                                                                                                                                             |                         | x          | x           | 1.0       |
| 8.7                         | Verify that the application does not log sensitive data as defined under local privacy laws or regulations, organizational sensitive data as defined by a risk assessment, or sensitive authentication data that could assist an attacker, including user’s session identifiers, passwords, hashes, or API tokens.                                             |                         | x          | x           | 3.0       |
| 8.8                         | Verify that all non-printable symbols and field separators are properly encoded in log entries, to prevent log injection.                                                                                                                                                                                                                                      |                         |            | x           | 2.0       |
| 8.9                         | Verify that log fields from trusted and untrusted sources are distinguishable in log entries.                                                                                                                                                                                                                                                                  |                         |            | x           | 2.0       |
| 8.10                        | Verify that an audit log or similar allows for non-repudiation of key transactions.                                                                                                                                                                                                                                                                            | x                       | x          | x           | 3.0       |
| 8.11                        | Verify that security logs have some form of integrity checking or controls to prevent unauthorized modification.                                                                                                                                                                                                                                               |                         |            | x           | 3.0       |
| 8.12                        | Verify that the logs are stored on a different partition than the application is running with proper log rotation.                                                                                                                                                                                                                                             |                         |            | x           | 3.0       |
| 8.13                        | Time sources should be synchronized to ensure logs have the correct time.                                                                                                                                                                                                                                                                                      | x                       | x          | x           | 3.0.1     |

### 8.1

Psono does not output any error messages or stack traces when `DEBUG` is set to `False`. Psono.pw is has `DEBUG: False`

### 8.2

All security controls deny access by default

### 8.3

All access requests including security controls log success and failure events as part of the audit logging feature in the EE version.
The event code is unique and allows to trace particular events.

### 8.4

All events have a unique timestamp. The server's healthcheck easily validates if the time of the server is in sync with
a configurable timeserver.

### 8.5

Psono does not provide a particular log viewing software. Log files can be parsed with an editor. Third party logging software
e.g. Splunk is supported because of the computer friendly json log format.

### 8.6

Its the administrators responsibility to ship logs to a secure location.

### 8.7

All log information is sanitized and does not include any private data nor any of the user’s session identifiers, passwords, hashes, or API tokens.

### 8.8

The logging format as json with pythons 'json.dumps()' makes it very hard for attackers to manipulate logging data.
The function by default ensures that all non-ASCII characters in the output are escaped with \uXXXX sequences and utf-8 encoding.

### 8.9

Currently only trusted data is logged, but plans are to add separate data to enrich the information provided.

### 8.10

All log requests in the audit log log the author of the request.

### 8.11

This has to be implemented by the administrator. We advise to ship log files to a secure third location to prevent
tempering with data. Professional logging software will ensure the integrity of the logs.

### 8.12

Easily configurable in the config and / or by specifying a mount partition on a separate device.

### 8.13

Psono helps here, by showing the current time sync status as healthcheck.

