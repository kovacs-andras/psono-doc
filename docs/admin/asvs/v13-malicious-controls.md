---
title: V13 Malicious Controls
metaTitle: V13 Malicious Controls | Psono Documentation
meta:
  - name: description
    content: Malicious controls verification requirements
---

# V13 Malicious Controls

Malicious controls verification requirements

[[toc]]

## ASVS Verification Requirement

| ID                          | Detailed Verification Requirement                                                                                                                                                                                                                                                                                                                              | Level 1                 | Level 2    | Level 3     | Since     |
|-----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|------------|-------------|-----------|
| 13.1                        | Verify all malicious activity is adequately sandboxed, containerized or isolated to delay and deter attackers from attacking other applications.                                                                                                                                                                                                               |                         |            | x           | 2.0       |
| 13.2                        | Verify that the application source code, and as many third party libraries as possible, does not contain back doors, Easter eggs, and logic flaws in authentication, access control, input validation, and the business logic of high value transactions.                                                                                                      |                         |            | x           | 3.0.1     |



### 13.1

There is no automatic process in place to automatically isolate any attacker. The application itself is designed to allow separation of individual components (e.g. client, server, fileserver, database) to limit possibilities of attackers and delay them. Each component can be hosted in a docker container. There are sandboxes available e.g. https://github.com/google/gvisor to run docker container with an even better form of abstraction.

### 13.2

There are currently no known back doors, easter eggs and logic flaws and whatever has been mentioned here. Vulnerability scanners are in place to alert us in case such flaws are discovered.
